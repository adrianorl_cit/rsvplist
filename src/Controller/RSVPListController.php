<?php

namespace Drupal\rsvplist\Controller;

/**
 * @file
 * Contains \Drupal\rsvplist\Controller\RSVPListController.
 */

use Drupal\Component\Utility\Html;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;

/**
 * Controller for RSVP List Controller.
 */
class RSVPListController extends ControllerBase {

  /**
   * Gets all RSVPs for all nodes.
   */
  protected function load() {
    $select = Database::getConnection()->select('rsvplist', 'r');
    // Join the users table, so we can get the entry creator's username.
    $select->join('users_field_data', 'u', 'r.uid = u.uid');
    // Join the node table, so we can get the event's name.
    $select->join('node_field_data', 'n', 'r.nid = n.nid');
    // Select these specific fields for the output.
    $select->addField('u', 'name', 'username');
    $select->addField('n', 'title');
    $select->addField('r', 'mail');
    $entries = $select->execute()->fetchAll(\PDO::FETCH_ASSOC);
    return $entries;
  }

  /**
   * Creates the report page.
   *
   * Render array for report output.
   */
  public function report() {
    $content = [];
    $content['message'] = [
      '#markup' => $this->t('Bellow is a list of all Event RSVPs including username, email address and the name of the event they will be attending'),
    ];

    $headers = [
      t('Name'),
      t('Event'),
      t('Email'),
    ];

    $rows = [];
    $entries = [];
    $entries = $this->load();
    foreach ($entries as $entry) {
      $rows[] = $entry;
    }

    $content['table'] = [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => t('No entries available.'),
    ];

    // Dont cache this page.
    $content['#cache']['max-age'] = 0;
    return $content;

  }

}
